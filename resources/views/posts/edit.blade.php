@extends('layout')

@section('content')
    <div class="container">
        <h1>Edit post</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container-fluid px-0">
            <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
        </div>
        <form action="{{ route('posts.update', $post->id) }}" method="POST" >
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $post->id }}">
            <span>Name</span>
            <input type="text" class="form-control" value="{{ $post->name }}" name="name" placeholder="Enter name ...">
            <span>Content</span>
            <input type="text" class="form-control" value="{{ $post->content }}" name="content" placeholder="Enter content ...">
            <span>Category</span>
            <select name="category_id" class="form-control">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"
                        {{ $category->id == $post->category_id ? 'selected' : '' }}>
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-success mt-1">Submit</button>
        </form>
    </div>
@endsection