@extends('layout')

@section('content')
    <pre>{{ dd($posts) }}</pre>
    <div class="container">
        <div class="container"><h1>Post</h1></div>
        <a class="btn btn-success mb-1" href="posts/create">Add</a>
        <table class="table table-stripped">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Content</td>
                <td>Category</td>
                <td>Actions</td>
            </tr>
            @foreach($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->name }}</td>
                    <td>{{ $post->content }}</td>
                    <td>{{ $post->category->name }}</td>
                    <td>
                        <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary" href="{{ route('posts.edit', $post->id) }}">Edit</a>
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $posts->links() }}
    </div>
@endsection
