@extends('layout')

@section('content')
    <div class="container">
        <h1>Add category</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container-fluid px-0">
            <a href="{{ route('categories.index') }}" class="btn btn-success">Back</a>
        </div>
        <form action="{{ route('categories.store') }}" method="POST" >
            @csrf
            <span>Name</span>
            <input type="text" class="form-control" name="name" placeholder="Enter name ...">
            <button type="submit" class="btn btn-success mt-1">Submit</button>
        </form>
    </div>
@endsection