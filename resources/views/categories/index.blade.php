@extends('layout')

@section('content')
    <div class="container">
        <div class="container"><h1>Category</h1></div>
        <a class="btn btn-success mb-1" href="{{ route('categories.create') }}">Add</a>
        <table class="table table-stripped">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Actions</td>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $categories->links() }}
    </div>
@endsection